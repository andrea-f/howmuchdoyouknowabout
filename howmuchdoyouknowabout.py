# To change this template, choose Tools | Templates
# and open the template in the editor.

#Code provided by www.dinx.tv code is under bsd license
__author__="Andrea Fassina"
__date__ ="$01-Oct-2014 23:34:47$"
from tools.api import Freebase, YouTube, ScoreVideo
import os,sys, traceback
from tools import fileHandler
from termcolor import colored

#CONFIG HOOKS
CONFIG_NAME = "HMDYKA_CONFIG"
USE_CSV = "USE_CSV"
DOWNLOAD_YOUTUBE = "DOWNLOAD_YOUTUBE"
VIDEO_QUERY = "VIDEO_QUERY"
SPLIT_QUERY = "split_query"
DOWNLOAD_TEXT = "DOWNLOAD_TEXT"
MAX_VIDEOS_PER_EVENT = "MAX_VIDEOS_PER_EVENT"
ROOT = "ROOT"
MIN_SCORE = "MIN_SCORE"
DURATION = "DURATION"
MAX_DURATION = "MAX_DURATION"
DEFAULTS = {
    "init_date":1950,
    "end_date": 2020,
    "inc": 10,
    "tp": "/film/film",
    "country": "Italy",
    "lang": "it",
    "limit": 50,
    "MAX_DURATION":999999999999999999999,
    "DURATION": 0,
    "MAX_VIDEOS_PER_EVENT": 2,
    "MIN_SCORE":0
}
REQUIRED = ["init_date", "end_date","country","lang","limit","tp","MAX_DURATION","DURATION","MAX_VIDEOS_PER_EVENT"]
CATEGORY = "tp"
class HMDYKA(object):
    def __init__(self, fn = ""):
        """"""
        self.freebase = Freebase.Freebase()
        self.fh = fileHandler.fileHandler()
        self.youtube = YouTube.SearchVideos()
        
        self.config = self.getInput(fn = fn)
        fn = self.checkRequired()
        self.fileName = fn


    def checkRequired(self):
        fn = ""
        for r in REQUIRED:
            if r not in self.config:
                self.config[r] = DEFAULTS[r]
                print "MISSING REQUIRED VALUE %s , using default: %s" %(r, DEFAULTS[r])
            else:
                fn += self.config[r]+"_"
        fn = fn.replace('/','')
        return os.getcwd()+"/"+fn


    def getInput(self, fn = ""):
        
        if len(fn)==0:
            fn = CONFIG_NAME
        lines = self.fh.readFileByLine(fileName = fn)
        config = {}
        for line in lines:
            if not "#" in line:
                l = line.split("=")
                key = l[0].strip()
                value = l[1].strip()
                config[key]=value
        return config


    def makeQueryFreebase(self, init_date = 1950, end_date = 2010, country = "Italy", limit = 50, lang = "it", tp = "/film/film", prop = "initial_release_date"):
        """Creates freebase query

        [{
          "/film/film/initial_release_date": null,
          "/film/film/initial_release_date<": "1970",
          "/film/film/initial_release_date>=": "1960",
          "/film/film/country": "Italy",
          "limit": 35,
          "mid": null,
          "type": "/film/film",
          "name": {
            "value": null,
            "lang": "/lang/it"
          }
        }]
        """
        return """[{
          \""""+tp+prop+"""\": null,
          \""""+tp+prop+"""<": \""""+end_date+"""\",
          \""""+tp+prop+""">=": \""""+init_date+"""\",
          \""""+tp+"""country": \""""+country+"""\",
          "limit": """+limit+""",
          "mid": null,
          "type": \""""+tp[:-1]+"""\",
          "name": {
            "value": null,
            "lang": "/lang/"""+lang+"""\"
          }
        }]"""

    def preProcessFreebase(self, results = []):
        """Calls makeQueryFreebase."""
        if SPLIT_QUERY in self.config:
                i = self.config["init_date"]
                while i < e:
                    qf = self.makeQueryFreebase(
                        init_date = i,
                        end_date = self.config['end_date'],
                        lang = self.config['lang'],
                        country = self.config['country'],
                        tp = self.config['tp'],
                        limit = self.config['limit']
                    )
                    print "Freebase query: %s" % qf
                    res = self.freebase.search(query_filter = qf)
                    i += DEFAULT_INC
                    results.append(res)
        else:
                qf = self.makeQueryFreebase(
                    init_date = self.config['init_date'],
                    end_date = self.config['end_date'],
                    lang = self.config['lang'],
                    country = self.config['country'],
                    tp = self.config['tp'],
                    limit = self.config['limit']
                )
                print "Freebase query: %s" % qf
                results.append(self.freebase.search(query_filter = qf))
        return results
 #      
    def run(self):
        results = []
        if DOWNLOAD_TEXT in self.config:
            results = self.preProcessFreebase()
        if DOWNLOAD_YOUTUBE in self.config:
            self.fileName = self.fileName+"_youtube"
            if USE_CSV in self.config:
                items = self.readDataFromCSV(self.config[USE_CSV])
                queries = self.convertCSVToQuery(items)
            elif VIDEO_QUERY in self.config:
                queries = self.config[VIDEO_QUERY]
                
            else:
                print "Please decide what to search using the VIDEO_QUERY='something something' or USE_CSV with a suitable CSV file, in CONFIG file."
                sys.exit(2)
            results = self.searchFeed(queries)
        return results

    def getVideoClips(self):
        """Retrieves video clips.

        Add to video metadata:
        `feed[X]` is the tuple of entry [0] and [1] query information
        `entry = feed[tot][0]`
        `q = feed[tot][1][0]`
        `eventName = feed[tot][1][1]`
        `date = feed[tot][1][2]`
        """

        try:
            ##### RUN YOUTUBE QUERY
            results = self.run()
            return results
        except Exception as e:
            print "[getVideoClips] ERROR: %s" % e
            exc_type, exc_value, exc_traceback = sys.exc_info()
            error = " Error whilst running HOST:\n\n%s" % traceback.format_exc(limit=10)
            print error
            sys.exit(2)

        
    def scoreFeed(self, feed, query):
        """Analyse feed.

        Saves videos in csv file.
        
        """
        o = 0
        videos = []        
        for entry in feed.entry:
            q = query[0]
            eventName = q
            item = query[1][0]
            date = query[1][1]
            category = query[2]
            root = query[3]
            videos.append(self.youtube.createVideoObject(
                entry = entry,
                items_name = [item],
                q = q,
                date = date,
                eventName = eventName,
                root = root,
                category = category
            ))
        vids = []
        top_vids = []
        for video in videos:
            score = 0
            params = {
                "duration": self.config[DURATION],
                "max_duration": self.config[MAX_DURATION]
            }
            st = ScoreVideo.ScoringTools(video, params = params)
            score += st.checkEventNameInTitle()
            score += st.itemInVideo()
            score += st.checkEventNameInTitle()
            score += st.checkVideoDuration()
            video.score = score
            print "\nVideo: %s\nEventName: %s\nScore: %s" % (video.title, video.eventName, video.score)            
            if int(video.score)>int(self.config[MIN_SCORE]):                                
                vids.append((video,int(video.score)))
        t = sorted(vids,key=lambda x: x[1])
        for v in t:
            if len(top_vids)<int(self.config[MAX_VIDEOS_PER_EVENT]):
                print len(top_vids)
                top_vids.append(v)

        for vi in top_vids:
            try:
                video_data = vi[0]
                print colored(("SAVING %s for %s" % (video_data.title, video_data.eventName)), "red")
                csvline = video_data.url +","+ video_data.items[0] + "," + video_data.date+"\n"
                saved = self.fh.saveFile(fileName = self.fileName, data = csvline)
                if saved is True:
                    o += 1
            except:
                pass
        print "Saved %s videos to %s. " % (o,self.fileName)
        return o

            
        



    def parseFilmResults(self):
        """Parses results from Freebase in JSON format, save as CSV."""
        try:
            ##### RUN FREEBASE QUERY/ES
            results = self.run()
        except Exception as e:
            print "[parseFilmResults] ERROR: %s" % e
            
            exc_type, exc_value, exc_traceback = sys.exc_info()
            error = " Error whilst running HOST:\n\n%s" % traceback.format_exc(limit=10)
            print error
            sys.exit(2)
        o = 0
        if not ".csv" in self.fileName:
            self.fileName +=".csv"        
        for r in results:
            for item in r:#TODO: maybe add limit [:5]:
                for key,value in item.iteritems():
                    if "initial_release_date" in key:
                        date=value               
                title = item['name']['value']
                print "[parseFilmResults] free base res: %s, %s "%(title, date)
                csvline = title.encode('utf8').replace(',','')+","+str(date)+"\n"
                #Save data
                saved = self.fh.saveFile(fileName = self.fileName, data = csvline)
                if saved is True:
                    o += 1
        print "[parseFilmResults] Saved %s items in %s" % (o,self.fileName)
        return self.fileName

    def readDataFromCSV(self,fileName = ""):
        """Reads query searches from local file as a list.

        Return list with line information
        """
        items = []
        if len(fileName) == 0:
            fileName = self.fileName
        lines = self.fh.readFileByLine(fileName = fileName)
        for line in lines:
            row = line.split(",")
            items.append(row)
        print "[readDataFromCSV] Read: %s rows." % len(items)
        return items


    def convertCSVToQuery(self,items = []):
        """Converts CSV to list of queries."""
        queries = []
        for item in items:
            #q = ' '.join(item)
            q = item[0] + " " + item[1][:4]
            queries.append((q,item, self.config[CATEGORY], self.config[ROOT]))
        return queries


    def searchFeed(self, queries = []):
        """Search either a list of items or a string of text.

        Return list with results feeds.
        """
        feeds = []
        for query in queries:
            feed = self.youtube.runYouTubeQuery(query = query[0])
            saved_total=self.scoreFeed(feed,query)
        return saved_total


if __name__ == "__main__":
    print "Hello World"
