# To change this template, choose Tools | Templates
# and open the template in the editor.

#Code provided by Andrea Fassina code is under bsd license
import unittest
from howmuchdoyouknowabout import HMDYKA
import os.path
class  TestFreebaseTestCase(unittest.TestCase):
    def setUp(self):
        self.hmdyka = HMDYKA()

    #def tearDown(self):
    #    self.foo.dispose()
    #    self.foo = None

    def testSearchDefault(self):
        fn = self.hmdyka.parseFilmResults()
        assert os.path.isfile(fn)

    def testSearchYouTube(self):
        fn = self.hmdyka.getVideoClips()
        assert os.path.isfile(fn)

if __name__ == '__main__':
    suite = unittest.TestSuite()
    #suite.addTest(TestFreebaseTestCase('testSearchDefault'))
    suite.addTest(TestFreebaseTestCase('testSearchYouTube'))
    unittest.TextTestRunner().run(suite)
