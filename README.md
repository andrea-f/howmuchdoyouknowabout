# README #

# Python Scraper for matching Freebase query with YouTube. #
## 1. - Config options are in HMDYKA_CONFIG ##
## 2. - If DOWNLOAD_TEXT is uncommented, Freebase query will be run and result saved in CSV file ##
## 3. - If DOWNLOAD_YOUTUBE is uncommented, CSV file specified in USE_CSV is loaded and each row is split and then searched on YouTube, then resulting video is matched with search query, if matches it is saved. Number of videos saved per event can be specified in MAX_VIDEOS_PER_EVENT in HMDYKA_CONFIG. ## 
## 4. - Check testFreebase.py to select which service to run. ## 
## 5. To test run testFreebase.py ##